module.exports = {
	root: true,
	env: {
		node: true,
	},
	extends: [
		'plugin:vue/essential',
		'@vue/airbnb',
	],
	rules: {
		'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
		'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
		'indent': [0, 'tab'],
		'no-tabs': 0,
		'max-len': 'off',
		"no-mixed-spaces-and-tabs": 0,
	},
	parserOptions: {
		parser: 'babel-eslint',
	},
};
