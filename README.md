# firebase-vue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Project repo contains
- Login.vue: The default view if a user isn't authenticated. Here a user will encounter log-in, sign-up, and forgot-password scenarios.
- Dashboard.vue: Where the user will create and read posts as well as interact with posts by commenting or liking them.
- Navigation.vue: This gives users a way to navigate between the different views in this project.
- Settings.vue: Where users can update their profiles.
