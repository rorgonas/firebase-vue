import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './assets/scss/app.scss';

const fb = require('./firebaseConfig.js');

Vue.config.productionTip = false;

let app;
fb.auth.onAuthStateChanged((user) => {
	console.log('User: ', user);
	if (!app) {
		app = new Vue({
			el: '#app',
			router,
			store,
			render: h => h(App),
		});
	}
	if (user) {
		store.commit('setCurrentUser', user);
		store.dispatch('fetchUserProfile');
		fb.usersCollection.doc(user.uid)
			.onSnapshot((doc) => {
				store.commit('setUserProfile', doc.data());
			});
		fb.postsCollection.orderBy('createdOn', 'desc')
			.onSnapshot((querySnapshot) => {
				// check if created by currentUser
				let createdByCurrentUser;
				if (querySnapshot.docs.length) {
					createdByCurrentUser = store.state.currentUser.uid === querySnapshot.docs[0].data().userId;
				}
				// add new posts to hiddenPosts array after initial load
				if (querySnapshot.docChanges().length !== querySnapshot.docs.length && querySnapshot.docChanges()[0].type === 'added' && !createdByCurrentUser) {
					const post = querySnapshot.docChanges()[0].doc.data();
					post.id = querySnapshot.docChanges()[0].doc.id;
					store.commit('setHiddenPosts', post);
				} else {
					const postsArray = [];
					querySnapshot.forEach((doc) => {
						const post = doc.data();
						post.id = doc.id;
						postsArray.push(post);
					});
					store.commit('setPosts', postsArray);
				}
			});
	}
});
