import Vue from 'vue';
import VueRouter from 'vue-router';
import firebase from 'firebase';

import Login from '@/components/Login.vue';
import Dashboard from '@/components/Dashboard.vue';
import Settings from '@/components/Settings.vue';

Vue.use(VueRouter);

const routes = [{
	path: '*',
	redirect: '/dashboard',
}, {
	path: '/login',
	name: 'Login',
	component: Login,
}, {
	path: '/dashboard',
	name: 'Dashboard',
	component: Dashboard,
	meta: {
		requiresAuth: true,
	},
}, {
	path: '/settings',
	name: 'Settings',
	component: Settings,
	meta: {
		requiresAuth: true,
	},
}];

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes,
});

router.beforeEach((to, from, next) => {
	const requiresAuth = to.matched.some(x => x.meta.requiresAuth);
	const { currentUser } = firebase.auth();
	if (requiresAuth && !currentUser) {
		next('/login');
	} else if (requiresAuth && currentUser) {
		next();
	} else {
		next();
	}
});

export default router;
