import Vue from 'vue';
import Vuex from 'vuex';

const fb = require('./../firebaseConfig.js');

Vue.use(Vuex);

// handle page reload
/**
 * placed on main.js temp
 * */

export default new Vuex.Store({
	state: {
		currentUser: null,
		userProfile: {},
		posts: [],
		hiddenPosts: [],
	},
	mutations: {
		setCurrentUser(state, val) {
			state.currentUser = val;
		},
		setUserProfile(state, val) {
			state.userProfile = val;
		},
		setPosts(state, val) {
			if (val) {
				state.posts = val;
			} else {
				state.posts = [];
			}
		},
		setHiddenPosts(state, val) {
			if (val) {
				if (!state.hiddenPosts.some(x => x.id === val.id)) {
					state.hiddenPosts.unshift(val);
				}
			} else {
				state.hiddenPosts = [];
			}
		},
	},
	actions: {
		clearData({ commit }) {
			commit('setCurrentUser', null);
			commit('setUserProfile', {});
			commit('setPosts', []);
		},
		fetchUserProfile({ commit, state }) {
			fb.usersCollection.doc(state.currentUser.uid)
				.get()
				.then((res) => {
					if (res.exists) {
						commit('setUserProfile', res.data());
					} else {
						// doc.data() will be undefined in this case
						console.log('No such document!');
					}
				})
				.catch((err) => {
					console.log(err);
				});
		},
		updateProfile({ state }, data) {
			const { name, title } = data;
			fb.usersCollection.doc(state.currentUser.uid)
				.update({
					name,
					title,
				})
				.then((user) => {
					// update all posts by user to reflect new name
					console.log('User: ', user);
					fb.postsCollection.where('userId', '==', state.currentUser.uid)
						.get()
						.then((docs) => {
							docs.forEach((doc) => {
								fb.postsCollection.doc(doc.id)
									.update({
										userName: name,
									});
							});
						});
					// update all comments by user to reflect new name
					fb.commentsCollection.where('userId', '==', state.currentUser.uid)
						.get()
						.then((docs) => {
							docs.forEach((doc) => {
								fb.commentsCollection.doc(doc.id)
									.update({
										userName: name,
									});
							});
						});
				})
				.catch((err) => {
					console.log(err);
				});
		},
	},
	modules: {},
});
